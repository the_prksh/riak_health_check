
package main

type Server_health_status struct {
  
    Status_list []*Health_check_result
}

type Health_check_result struct {
  
  Name          string
  Status        string
  Description   string
  
}

const (

  Cpu_utilization_warning_threshold   = 90.0
  Memory_usage_warning_threshold      = 90.0
  Swap_memory_usage_warning_threshold = 90.0
  Disk_usage_health_status            = 75.0
  
  MAX_PROCESS   = 100000
  MAX_NODE_PUT_FSM_REJECTED           = 100
  MAX_NODE_GET_FSM_SIBLINGS_MEAN      = 100
  MAX_NODE_GET_FSM_OBJECT_SIZE_MEAN   = 100
  MAX_RIAK_SEARCH_VNODEQ_MEAN         = 100
  MAX_SEARCH_INDEX_FAIL_ONE           = 100
  MAX_READ_REPAIRS                    = 100
  MAX_NODE_GET_FSM_REJECTED           = 100
)

const Riak_url  = "http://127.0.0.1:8098/stats"