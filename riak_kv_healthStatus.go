/* 
    sys_process_count
    node_get_fsm_siblings_mean
    node_get_fsm_objsize_mean
    riak_search_vnodeq_mean
    search_index_fail_one
    read_repairs
    node_get_fsm_rejected 
    node_put_fsm_rejected
*/
package main

import (
	"encoding/json"
//	"fmt"
	"net/http"
	"strconv"
)

type riak_kv_status struct {
  
  Process_count                 uint64    `json:"sys_process_count"`
  Node_get_fsm_siblings_mean    uint64    `json:"node_get_fsm_siblings_mean"`
  Node_get_fsm_objsize_mean     uint64    `json:"node_get_fsm_objsize_mean"`
  Riak_search_vnodeq_mean       uint64    `json:"riak_search_vnodeq_mean"`
  Search_index_fail_one         uint64    `json:"search_index_fail_one"`
  Read_repairs                  uint64    `json:"read_repairs"`
  Node_get_fsm_rejected         uint64    `json:"node_get_fsm_rejected"`
  Node_put_fsm_rejected         uint64    `json:"node_put_fsm_rejected"`
}

func data_acquisition( accquire_data *riak_kv_status ) error {

	resp, err := http.Get(Riak_url)
	if err != nil {
		return err
	}

	if err := json.NewDecoder(resp.Body).Decode(accquire_data); err != nil {
		resp.Body.Close()
		return err
	}
	resp.Body.Close()
	
	return nil
}

func Get_riak_health_status( status []Health_check_result ) error {

	accquire_data := riak_kv_status{}
	
	err	:= data_acquisition( &accquire_data )
	if err != nil {
		return err
	}

	//fmt.Printf("\n\n Accquire Data : %v", accquire_data )

	// Process Count
	status[0].Name = "System Process Count"
	
	if accquire_data.Process_count >= MAX_PROCESS {
		status[0].Status	= "warning"	
	} else {
		status[0].Status	= "healthy"
	}
	
	status[0].Description	=	"Total number of process :"+strconv.FormatUint(accquire_data.Process_count, 10)	
	
  //  node_get_fsm_siblings_mean
	status[1].Name = "Riak Siblings encountered"
	
	if accquire_data.Node_get_fsm_siblings_mean >= MAX_NODE_GET_FSM_SIBLINGS_MEAN {
		status[1].Status	= "warning"	
	} else {
		status[1].Status	= "healthy"
	}
	
	status[1].Description	=	"Mean number of siblings encountered during all GET operations by this node within the last minute. "+
													"Current Get FSM siblings (Mean) :"+strconv.FormatUint(accquire_data.Node_get_fsm_siblings_mean, 10)	
	
  //  node_get_fsm_objsize_mean
  status[2].Name = "Riak Object Size"
	
	if accquire_data.Node_get_fsm_objsize_mean >= MAX_NODE_GET_FSM_OBJECT_SIZE_MEAN {
		status[2].Status	= "warning"	
	} else {
		status[2].Status	= "healthy"
	}
	
	status[2].Description	=	"Mean object size (bytes) encountered by this node within the last minute. "+
													"Object Size (mean) :"+strconv.FormatUint(accquire_data.Node_get_fsm_objsize_mean, 10)	
	
	//  riak_search_vnodeq_mean
	status[3].Name = "Message Queue health status"
	
	if accquire_data.Riak_search_vnodeq_mean >= MAX_RIAK_SEARCH_VNODEQ_MEAN {
		status[3].Status	= "warning"	
	} else {
		status[3].Status	= "healthy"
	}
	
	status[3].Description	=	"Mean number of unprocessed messages all vnode message queues in the Riak Search subsystem have received on this node in the last minute. "+
													"Unprocessed Messages (mean) :"+strconv.FormatUint(accquire_data.Riak_search_vnodeq_mean, 10)	
	
	//  search_index_fail_one
  status[4].Name = "Indexing decument health status`"
	
	if accquire_data.Search_index_fail_one >= MAX_SEARCH_INDEX_FAIL_ONE {
		status[4].Status	= "warning"	
	} else {
		status[4].Status	= "healthy"
	}
	
	status[4].Description	=	"Number of “Failed to index document” errors Search encountered for the last minute. "+
													"Number of Errors :"+strconv.FormatUint(accquire_data.Search_index_fail_one, 10)	
	
	//  read_repairs
  status[5].Name = "Read health status"
	
	if accquire_data.Read_repairs >= MAX_READ_REPAIRS {
		status[5].Status	= "warning"	
	} else {
		status[5].Status	= "healthy"
	}
	
	status[5].Description	=	"Number of read repair operations this node has coordinated in the last minute. "+
													"Number of Operations :"+strconv.FormatUint(accquire_data.Read_repairs, 10)	
	
	//  node_get_fsm_rejected 
  status[6].Name = "Get FSM health status"
	
	if accquire_data.Node_get_fsm_rejected >= MAX_NODE_GET_FSM_REJECTED {
		status[6].Status	= "warning"	
	} else {
		status[6].Status	= "healthy"
	}
	
	status[6].Description	=	"Number of GET FSMs actively being rejected by Sidejob’s overload protection. "+
													"Number of GET FSMs :"+strconv.FormatUint(accquire_data.Node_get_fsm_rejected, 10)	
	
	//  node_put_fsm_rejected
	status[7].Name = "Put FSM health status"
	
	if accquire_data.Node_put_fsm_rejected >= MAX_NODE_PUT_FSM_REJECTED {
		status[7].Status	= "warning"	
	} else {
		status[7].Status	= "healthy"
	}
	
	status[7].Description	=	"Number of PUT FSMs actively being rejected by Sidejob’s overload protection. "+
													"Number of PUT FSMs :"+strconv.FormatUint(accquire_data.Node_put_fsm_rejected, 10)
	
	return nil
}