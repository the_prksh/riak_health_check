package main

import ( 
          "fmt"
					"encoding/json"
)

func main() {
	
	health_status := [12]Health_check_result{}
	
	if err := Get_cpu_health_status(&health_status[0]); err != nil {
		fmt.Printf( "\n\n Error : %v\n\n", err )
	}
	
	if err := Get_memory_health_status(&health_status[1]); err != nil {
		fmt.Printf( "\n\n Error : %v\n\n", err )
	}

	if err := Get_swap_health_status(&health_status[2]); err != nil {
		fmt.Printf( "\n\n Error : %v\n\n", err )
	}
	
	if err := Get_disk_health_status(&health_status[3]); err != nil {
		fmt.Printf( "\n\n Error : %v\n\n", err )
	}
	
	if err := Get_riak_health_status( health_status[4:] ); err != nil {
		fmt.Printf( "\n\n Error : %v\n\n", err )
	}
	
	data, err := json.MarshalIndent(health_status, "", "")
	if err != nil {
		fmt.Printf("\n Error : Json MarshalIndent failed.")
	}
	
	fmt.Printf( "\nHealth Status : \n%s\n", data )
	
}


/*
References :

https://golang.org/pkg/go/build/#hdr-Build_Constraints
https://dave.cheney.net/2013/10/12/how-to-use-conditional-compilation-with-the-go-build-tool
https://godoc.org/golang.org/x/sys/unix#Sysinfo
https://github.com/cloudfoundry/gosigar
https://bitbucket.org/bertimus9/systemstat

http://stackoverflow.com/questions/23367857/accurate-calculation-of-cpu-usage-given-in-percentage-in-linux

*/